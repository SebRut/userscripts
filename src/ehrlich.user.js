// ==UserScript==
// @name         Ehrlich
// @version      1
// @description  try to take over the world!
// @author       You
// @match        https://*/**
// @grant        none
// ==/UserScript==

const dict = [
    ["Lobbyismus", "Korruption"],
];

(function() {
    'use strict';

    const treeWalker = document.createTreeWalker(document.body, NodeFilter.SHOW_TEXT, (node) => node.nodeValue.trim() ? NodeFilter.FILTER_ACCEPT : NodeFilter.FILTER_SKIP);

    while (treeWalker.nextNode()) {
        const node = treeWalker.currentNode;

        var text = node.nodeValue;

        dict.forEach((entry) => {
            let target = entry[0];
            let replacement = entry[1];
            text = text.replace(target, replacement);
        });

        node.nodeValue = text;
    }
})();